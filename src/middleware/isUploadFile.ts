import path from 'path';
import multer from 'multer';

const storage = multer.diskStorage({
  destination: path.join(process.cwd(), '/uploads'),
  filename: (_, file, cb) => {
    // agrega la extencion al archivo
    cb(null, `${file.fieldname}__${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({
  storage,
});

export default upload;
