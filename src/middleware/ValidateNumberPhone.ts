const phoneValidator = /^(6|7)([0-9]{3}[ -]?)([0-9]{4})$/;

const validateNumberPhone = (phone: string) => {
  if (!phoneValidator.test(phone)) {
    return false;
  }

  return true;
};

export default validateNumberPhone;
