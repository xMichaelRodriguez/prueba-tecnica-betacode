import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';

const validateInputs = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) return res.status(400).jsonp([errors.mapped()]);

  return next();
};
export default validateInputs;
