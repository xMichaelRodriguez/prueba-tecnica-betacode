/* eslint-disable no-console */
import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';

const { SECRET_JWT_SEED } = process.env;
const newSecretJwtSeed = String(SECRET_JWT_SEED);
const jwtValidator = (req: Request, res: Response, next: NextFunction) => {
  // token headers
  const token = req.headers.authorization?.split(' ').pop();
  if (!token) {
    return res.status(401).json(['No hay Bearer Token en las headers']);
  }

  try {
    const decoded: string | object = jwt.verify(token, newSecretJwtSeed);

    req.user = decoded;
  } catch (error) {
    // console.log(error);
    return res.status(401).jsonp(['Token no válido']);
  }

  return next();
};

export default jwtValidator;
