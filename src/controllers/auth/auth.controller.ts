import { Request, Response } from 'express';

import { IErrorOrSuccessAuth, IRegister, IResponse } from '../../interfaces/auth.interface';
import generateJWT from '../../lib/jwt';
import validTypeUser from '../../lib/typeUserValidator';
import Auth from '../../models/auth/Auth.model';
import AuthLogin from '../../models/auth/AuthLogin.model';

const getProfile = async (req: Request, res: Response) => {
  const { user } = req;
  const objLogin = new AuthLogin();
  try {
    const result = await objLogin.find(user.uid);

    if (result.error) {
      return res.status(404).jsonp(result.error);
    }

    return res.status(200).jsonp(result.maestro);
  } catch (error) {
    return res.status(500).jsonp(String(error));
  }
};
const register = async (req: Request, res: Response) => {
  const { course, name, email, password, classRoom }: IRegister = req.body;

  const objAuth = new Auth(name, course, email, password, classRoom);

  try {
    const resp: IErrorOrSuccessAuth = await objAuth.save();

    if (!resp.ok) {
      return res.status(400).json({
        ok: resp.ok,
        msg: resp.msg,
      });
    }

    const uid = validTypeUser(resp);
    const token = await generateJWT({ uid });
    return res.status(201).jsonp({
      ok: true,
      msg: 'user Saved',
      data: resp.user,
      token,
    });
  } catch (error) {
    // eslint-disable-next-line no-console
    return res.status(500).jsonp('Algo salio mal');
  }
};

const login = async (req: Request, res: Response) => {
  const { email, password } = req.body;
  const objAuthLogin = new AuthLogin();
  try {
    const { found, valid, user }: IResponse = await objAuthLogin.login({ email, password });

    if (!found) {
      return res.status(404).jsonp(['usuario no encontrado']);
    }

    if (!valid) {
      return res.status(401).jsonp(['credenciales incorrectas']);
    }
    if (user === null) {
      return res.status(404).jsonp(['usuario no encontrado']);
    }

    const uid = user.id;
    const token = await generateJWT({ uid });

    return res.status(200).json({
      user,
      token,
    });
  } catch (error) {
    return res.status(500).jsonp('Algo salio mal');
  }
};

export default {
  getProfile,
  login,
  register,
};
