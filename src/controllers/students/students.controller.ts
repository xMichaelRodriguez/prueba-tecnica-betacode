/* eslint-disable no-console */
import { Request, Response } from 'express';
import { IPublicStudent, IStudent } from '../../interfaces/student.interface';
import Student from '../../models/students/student.model';

const register = async (req: Request, res: Response) => {
  const { user } = req;
  const { name, email, phone, dutyManager } = req.body;
  const objStudent = new Student(name, email, phone, dutyManager);
  try {
    const result: IStudent = await objStudent.save({ idTeacher: user.uid });

    return res.status(201).jsonp([result]);
  } catch (error) {
    const newError = String(error);

    return res.status(500).jsonp([newError]);
  }
};

const getStudents = async (req: Request, res: Response) => {
  const { user } = req;

  try {
    const { found, students } = await Student.getStudents({
      idTeacher: user.uid,
    });
    if (!found) {
      return res.status(404).jsonp(['estudiantes no encontrados']);
    }

    return res.status(200).jsonp([...students]);
  } catch (error) {
    const err = String(error);
    return res.status(500).jsonp([err]);
  }
};

const findById = async (req: Request, res: Response) => {
  const { user } = req;
  const { student } = req.params;

  try {
    const { found, students } = await Student.findById({
      idTeacher: user.uid,
      studentId: student,
    });
    if (!found) {
      return res.status(404).jsonp(['estudiantes no encontrados']);
    }

    return res.status(200).jsonp([...students]);
  } catch (error) {
    const err = String(error);
    return res.status(500).jsonp([err]);
  }
};

const updateStudents = async (req: Request, res: Response) => {
  const { student } = req.params;
  const { user } = req;
  const { name, email, phone, dutyManager }: IPublicStudent = req.body;
  const objStudentUpdate = new Student(name, email, phone, dutyManager);
  try {
    const { state, message } = await objStudentUpdate.updateStudent({
      idTeacher: user.uid,
      studentId: student,
    });
    if (!state) {
      return res.status(400).json({ message });
    }

    return res.status(204).jsonp();
  } catch (error) {
    return res.status(500).jsonp([error]);
  }
};

const deleteStudent = async (req: Request, res: Response) => {
  const { student } = req.params;
  const { user } = req;

  try {
    await Student.deleteStudent({ idTeacher: user.uid, studentId: student });
    return res.status(204).json();
  } catch (err) {
    const parsedError = String(err);
    return res.status(400).jsonp([parsedError]);
  }
};

export default {
  findById,
  getStudents,
  register,
  updateStudents,
  deleteStudent,
};
