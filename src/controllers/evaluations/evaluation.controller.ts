import { Request, Response } from 'express';

import { IEvaluation } from '../../interfaces/indext';
import ExcelFileManager, { IEvaluationSheet } from '../../lib/excelFilleManager';
import Evaluations from '../../models/evaluations/evaluantions.model';

const getEvaluationsForTeacher = async (req: Request, res: Response) => {
  const { user } = req;
  const objEvaluation = new Evaluations(user.uid, '');

  try {
    const result = await objEvaluation.find();
    if (result.error) {
      return res.status(400).jsonp(...result.error);
    }
    return res.status(200).jsonp(result.students);
  } catch (error) {
    return res.status(500).jsonp(String(error));
  }
};

const uploadEvaluations = async (req: Request, res: Response) => {
  try {
    if (req.file) {
      const file = req.file.path;
      const objExcelManager = new ExcelFileManager(file);

      const sheetParsed: IEvaluationSheet[] = await objExcelManager.parseToJSON();
      const { id_maestro: idMaestro, id_alumno: idAlumno } = sheetParsed[0];
      const objEvaluation = new Evaluations(idMaestro, idAlumno);

      const result = await objEvaluation.saveMultipleEvaluations(sheetParsed);

      if (result.error) {
        return res.status(400).jsonp(result);
      }
      return res.status(201).jsonp(result.msg);
    }

    return res.status(400).jsonp('No se envio ningun archivo excel');
  } catch (error) {
    return new Error(String(error));
  }
};

const registerEvalation = async (req: Request, res: Response) => {
  const { user } = req;
  const { student } = req.params;

  const { evaluation1, evaluation2, evaluation3, evaluation4 }: IEvaluation = req.body;
  const objEvaluation = new Evaluations(user.uid, student);
  try {
    const evaluationRegistered = await objEvaluation.save({
      evaluation1,
      evaluation2,
      evaluation3,
      evaluation4,
    });
    if (evaluationRegistered.error) {
      return res.status(401).jsonp([evaluationRegistered.error]);
    }

    return res.status(201).jsonp([evaluationRegistered.evaluation]);
  } catch (error) {
    return res.status(500).jsonp([String(error)]);
  }
};

const updateEvaluation = async (req: Request, res: Response) => {
  const { user } = req;
  const { student } = req.params;
  const { evaluation1, evaluation2, evaluation3, evaluation4 } = req.body;
  const objEvaluation = new Evaluations(user.uid, student);
  try {
    const resp = await objEvaluation.updateEvaluationStudent({
      evaluation1,
      evaluation2,
      evaluation3,
      evaluation4,
    });
    if (resp.error) {
      return res.status(401).jsonp([resp.error]);
    }
    return res.status(200).json();
  } catch (error) {
    return res.status(500).jsonp([String(error)]);
  }
};

export default {
  getEvaluationsForTeacher,
  registerEvalation,
  updateEvaluation,
  uploadEvaluations,
};
