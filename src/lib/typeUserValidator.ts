import { IErrorOrSuccessAuth } from '../interfaces/indext';

export default function validTypeUser(resp: IErrorOrSuccessAuth) {
  let uid = '';
  const { user } = resp;
  if (user !== undefined) {
    uid = user.id;
  }
  return uid;
}
