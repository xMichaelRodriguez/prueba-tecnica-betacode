import reader from 'xlsx';
import fsExtra from 'fs-extra';

export interface IEvaluationSheet {
  id_alumno: string;
  id_maestro: string;
  evaluacion1: number;
  evaluacion2: number;
  evaluacion3: number;
  evaluacion4: number;
  nota: number;
}
export default class ExcelFileManager {
  constructor(public pathFile: string) {
    this.pathFile = pathFile;
  }

  /**
   * read the excel file provided in the class parameter (file path).
      then the data is transformed with sheet_to_json
      passing the evaluation sheet that is inside the excel workbook taking it out of file.sheetName.
      later they are mapped to a text string to later map them in json to be able to write the typed json of the data
   * @returns IEvaluetionSheet[]
   */
  parseToJSON = async () => {
    const file = await reader.readFile(this.pathFile);
    let data = [];
    const sheets = file.SheetNames;

    data = await reader.utils.sheet_to_json(file.Sheets[sheets[0]]);

    const evaluationSheet = data.map((evaluation) => JSON.stringify(evaluation));
    const evaluationSheetParsed: IEvaluationSheet[] = evaluationSheet.map((evaluation) =>
      JSON.parse(evaluation)
    );
    fsExtra
      .remove(this.pathFile)
      .then(() => {
        // eslint-disable-next-line no-console
        console.log('success!');
      })
      .catch((err: Error) => {
        throw new Error(String(err.message));
      });
    return evaluationSheetParsed;
  };
}
