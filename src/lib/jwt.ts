/* eslint-disable no-console */
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();
interface Iprops {
  uid: string;
}
const newSecret = String(process.env.SECRET_JWT_SEED);
const generateJWT = ({ uid }: Iprops) => {
  const payload = { uid };
  return new Promise((resolve, reject) => {
    jwt.sign(payload, newSecret, { expiresIn: '2h' }, (err, token) => {
      if (err) {
        // eslint-disable-next-line no-console
        console.log(err);
        reject(new Error('No se pudo generar el token'));
      }

      resolve(token);
    });
  });
};

export default generateJWT;
