import { JwtPayload } from 'jsonwebtoken';

export default function isJWTPayload(value: JwtPayload | string): value is JwtPayload {
  return (value as JwtPayload).id !== undefined;
}
