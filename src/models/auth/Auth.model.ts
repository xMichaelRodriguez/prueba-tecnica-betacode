import bycript from 'bcrypt';
import { QueryResult } from 'pg';
import { IErrorOrSuccessAuth } from '../../interfaces/auth.interface';
import queryHandler from '../../storage/queryHandler';

export default class Auth {
  constructor(
    public name: string,
    public course: string,
    public email: string,
    public password: string,
    public classRoom: number
  ) {
    this.name = name;
    this.course = course;
    this.email = email;
    this.password = password;
    this.classRoom = classRoom;
  }

  save = async (): Promise<IErrorOrSuccessAuth> => {
    const query =
      'INSERT INTO maestros(nombre,materia,email,aula,password) VALUES ($1,$2,$3,$4,$5) RETURNING  *';

    try {
      const result = await this.findIsExistUser();
      if (result.errorMessage !== '') {
        return { ok: false, msg: result.errorMessage };
      }

      const encryptedPassword = this.hashPassword(this.password);
      const values = [this.name, this.course, this.email, this.classRoom, encryptedPassword];
      const userSaved = await queryHandler.exect(query, [...values]);
      return this.saveValidation(userSaved);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      throw new Error('error al guardar usuario');
    }
  };

  saveValidation = (userSaved: QueryResult): IErrorOrSuccessAuth => {
    if (userSaved.rowCount !== 1) {
      return { ok: false, msg: 'no se pudo guardar el usuario' };
    }
    return { ok: true, msg: 'usuario guardado', user: userSaved.rows[0] };
  };

  findIsExistUser = async () => {
    const query = 'SELECT maestros.email from maestros WHERE email=$1;';
    const values = [this.email];
    try {
      const queryResult = await queryHandler.exect(query, values);
      if (queryResult.rows.length === 1) {
        return {
          errorMessage: `No se puede guardar el maetro porque ya existe uno con este correo: ${this.email}`,
        };
      }
      return { errorMessage: '' };
    } catch (error) {
      return { errorMessage: 'Algo salio mal' };
    }
  };

  private hashPassword = (textPlaint: string): string => bycript.hashSync(textPlaint, 10);
}
