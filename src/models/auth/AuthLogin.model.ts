import bcrypt from 'bcrypt';

import { ILogin, IResponse } from '../../interfaces/indext';
import queryHandler from '../../storage/queryHandler';

export default class AuthLogin {
  login = async ({ email, password }: ILogin) => {
    const result: IResponse = await this.findByEmailAndComparePassword({ email, password });

    return result;
  };

  findByEmailAndComparePassword = async ({ email, password }: ILogin): Promise<IResponse> => {
    try {
      const result = await queryHandler.exect('SELECT * FROM maestros WHERE email=$1', [email]);

      if (result.rowCount === 0) return { found: false, valid: false, user: null };

      const valid = await this.isPasswordValid(password, result.rows[0].password);
      if (!valid) {
        return { found: true, valid, user: null };
      }

      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password: notpass, ...rest } = result.rows.pop();
      return { found: true, valid, user: rest };
    } catch (err) {
      throw new Error(String(err));
    }
  };

  private isPasswordValid = async (password: string, hash: string): Promise<boolean> => {
    try {
      const valid = await bcrypt.compare(password, hash);
      return valid;
    } catch (err) {
      throw new Error('algo salio mal :(');
    }
  };

  find = async (id: string) => {
    const query = 'SELECT * FROM maestros WHERE id=$1 ;';
    const values = [id];

    try {
      const queryResult = await queryHandler.exect(query, values);

      if (queryResult.rows.length === 0) {
        return { error: 'Maestro no encontrado' };
      }
      return { maestro: queryResult.rows };
    } catch (error) {
      throw new Error('No se pudo realizar la peticion');
    }
  };
}
