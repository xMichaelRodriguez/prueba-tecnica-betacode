import { IPublicStudentSpanish, IStudent } from '../../interfaces/student.interface';
import queryHandler from '../../storage/queryHandler';

interface ITeacherId {
  idTeacher: string;
}
interface ITeacherAndStudent extends ITeacherId {
  studentId: string;
}
export default class Student {
  constructor(
    public name: string,
    public email: string,
    public phone: string,
    public dutyManager: string
  ) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.dutyManager = dutyManager;
  }

  static hydrateStudent = (students: IPublicStudentSpanish[]) =>
    students.map((student) => ({
      id: student.id,
      nombre: student.nombre,
      correo: student.email,
      telefono: student.tel,
      encargado: student.encargado,
    }));

  public save = async ({ idTeacher }: ITeacherId): Promise<IStudent> => {
    const query =
      'INSERT INTO estudiantes(nombre,email,tel,encargado,id_maestro) VALUES ($1, $2, $3,$4, $5) RETURNING *';
    const values = [this.name, this.email, this.phone, this.dutyManager, idTeacher];

    try {
      const studentToSave = await queryHandler.exect(query, values);

      if (studentToSave.rows.length === 0) throw new Error('NO se pudo guardar el estudainte');
      const studentSaved: IStudent = studentToSave.rows.pop();

      return studentSaved;
    } catch (error) {
      throw new Error('Algo salio mal');
    }
  };

  static findById = async ({
    idTeacher,
    studentId,
  }: // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ITeacherAndStudent): Promise<{ found: boolean; students: any }> => {
    const query = 'SELECT * FROM estudiantes WHERE estudiantes.id_maestro=$1 AND estudiantes.id=$2';
    const values = [idTeacher, studentId];

    try {
      const queryResult = await queryHandler.exect(query, values);
      if (queryResult.rows.length === 0) {
        return { found: false, students: {} };
      }
      const students = this.hydrateStudent(queryResult.rows);
      return { found: true, students };
    } catch (error) {
      throw new Error('Algo salio mal');
    }
  };

  static getStudents = async ({
    idTeacher,
  }: // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ITeacherId): Promise<{ found: boolean; students: any }> => {
    const query = 'SELECT * FROM estudiantes WHERE estudiantes.id_maestro=$1 ';
    const values = [idTeacher];

    try {
      const queryResult = await queryHandler.exect(query, values);

      if (queryResult.rows.length === 0) {
        return { found: false, students: [] };
      }
      const students = this.hydrateStudent(queryResult.rows);

      return { found: true, students };
    } catch (error) {
      throw new Error('Algo salio mal');
    }
  };

  static deleteStudent = async ({ idTeacher, studentId }: ITeacherAndStudent): Promise<void> => {
    const query = 'DELETE FROM estudiantes WHERE estudiantes.id=$1 AND estudiantes.id_maestro=$2';
    const values = [studentId, idTeacher];

    try {
      await queryHandler.exect(query, [...values]);
    } catch (error) {
      throw new Error('No se pudo eliminar el estudiante');
    }
  };

  updateStudent = async ({ idTeacher, studentId }: ITeacherAndStudent) => {
    const query =
      'UPDATE estudiantes SET nombre=$1,email=$2, tel=$3,encargado=$4 WHERE id_maestro=$5 AND id=$6';
    const values = [this.name, this.email, this.phone, this.dutyManager, idTeacher, studentId];

    try {
      const queryResult = await queryHandler.exect(query, values);

      if (queryResult.rowCount === 0)
        return { state: false, message: 'No se pudo actualizar el estudiante' };

      return { state: true, message: 'Estudiante actualizado' };
    } catch (error) {
      throw new Error('Algo Salio mal');
    }
  };
}
