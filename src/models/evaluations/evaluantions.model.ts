import { IEvaluation } from '../../interfaces/indext';
import { IEvaluationSheet } from '../../lib/excelFilleManager';
import queryHandler from '../../storage/queryHandler';

interface IData {
  query: string;
  values: (string | number)[];
}
export default class Evaluations {
  constructor(public idTeacher: string, public idStudent: string) {
    this.idStudent = idStudent;
    this.idTeacher = idTeacher;
  }

  find = async () => {
    const query = `
      SELECT estudiantes.nombre,maestros.nombre as maestro,evaluaciones.evaluacion_1,
      evaluaciones.evaluacion_2,evaluaciones.evaluacion_3,evaluaciones.evaluacion_4,evaluaciones.nota
      FROM estudiantes JOIN maestros
      ON estudiantes.id_maestro=maestros.id 
      JOIN evaluaciones
      ON estudiantes.id=evaluaciones.id_alumno
      AND evaluaciones.id_maestro=maestros.id AND maestros.id=$1;`;
    const values = [this.idTeacher];

    try {
      const queryResult = await queryHandler.exect(query, values);

      if (queryResult.rows.length === 0) {
        return { students: [] };
      }

      return { students: queryResult.rows };
    } catch (error) {
      return { error: 'no se pudo realizar la peticion' };
    }
  };

  save = async ({ evaluation1, evaluation2, evaluation3, evaluation4 }: IEvaluation) => {
    const average = this.averageEvaluations({ evaluation1, evaluation2, evaluation3, evaluation4 });
    const query = `INSERT INTO evaluaciones(evaluacion_1,evaluacion_2,evaluacion_3,evaluacion_4,nota,id_alumno,id_maestro) 
      VALUES($1,$2,$3,$4,$5,$6,$7) RETURNING *`;

    const values = [
      evaluation1,
      evaluation2,
      evaluation3,
      evaluation4,
      average,
      this.idStudent,
      this.idTeacher,
    ];
    try {
      const result = await Evaluations.findByIdStudent({ id: this.idStudent });

      if (result.length === 1) {
        return {
          error:
            'no se pueden tener evaluaciones de estudiantes mas de 1 vez, es posible que desees actualizar las evaluaciones de un estudiante',
        };
      }
      const queryResult = await queryHandler.exect(query, values);
      return { evaluation: queryResult.rows };
    } catch (error) {
      throw new Error(String(error));
    }
  };

  /**
   *saves an array of evaluations,
   then it generates an array of the
    query handler so that at the end with a promise.all it saves said data
   * @param evaluations -> arr of evaluations interface
   */

  saveMultipleEvaluations = async (evaluations: IEvaluationSheet[]) => {
    try {
      const queries: IData[] | string = await this.prepareQueries(evaluations);

      if (typeof queries === 'string') {
        return { error: queries };
      }
      queries.forEach(async (query) => {
        await queryHandler.exect(query.query, query.values);
      });
      return { msg: 'Evaluaciones guardadas exitosamente' };
    } catch (err) {
      return { error: String(err) };
    }
  };

  /**
   * prepara un arreglo de consultas de insercion para cada registro que esta en el archivo excel
   * hace una promise.all de la llamada a la base de datos para verificar si existen estudiantes con
   * registro de notas previo para lanzar un error, si no hay ningun estudiante registrado de los que se
   * envian en el archivo excel este retorna un arreglo de consultas preparadas
   * @param evaluations -> arreglo de evaluaciones enviadas en el archivo excel
   * @returns -> arreglo de consultas preparadas
   */

  private prepareQueries = async (evaluations: IEvaluationSheet[]) => {
    const prepareQuery: IData[] = [];
    const query = `INSERT INTO evaluaciones(evaluacion_1,evaluacion_2,evaluacion_3,evaluacion_4,nota,id_alumno,id_maestro)
    VALUES($1,$2,$3,$4,$5,$6,$7)`;
    try {
      evaluations.forEach(
        ({
          evaluacion1,
          evaluacion2,
          evaluacion3,
          evaluacion4,
          nota,
          id_alumno: idAlumno,
          id_maestro: idMaestro,
        }) => {
          const values = [
            evaluacion1,
            evaluacion2,
            evaluacion3,
            evaluacion4,
            nota,
            idAlumno,
            idMaestro,
          ];
          prepareQuery.push({ query, values });
        }
      );

      const newEvaluations = evaluations.map((evaluation) =>
        Evaluations.findByIdStudent({ id: evaluation.id_alumno })
      );

      const isExistStudents = await Promise.all(newEvaluations);
      if (Object.values(isExistStudents)[0].length < 1) {
        return prepareQuery;
      }

      const arrDelete: Promise<void>[] = [];
      Object.values(isExistStudents).forEach((element) => {
        if (element[0].id !== undefined)
          arrDelete.push(this.deleteEvaluationForSheets({ idTeacher: element[0].id }));
      });

      await Promise.all(arrDelete);
      return prepareQuery;
    } catch (error) {
      throw new Error('no se pudo guardar los registros');
    }
  };

  deleteEvaluationForSheets = async ({ idTeacher }: { idTeacher: string }) => {
    const query = 'DELETE FROM evaluaciones WHERE evaluaciones.id=$1 ';
    const values = [idTeacher];
    try {
      await queryHandler.exect(query, values);
    } catch (error) {
      throw new Error('No se pudo eliminar el estudiante');
    }
  };

  updateEvaluationStudent = async ({
    evaluation1,
    evaluation2,
    evaluation3,
    evaluation4,
  }: IEvaluation) => {
    const average = this.averageEvaluations({
      evaluation1,
      evaluation2,
      evaluation3,
      evaluation4,
    });
    const query = `UPDATE evaluaciones
       SET evaluacion_1=$1, evaluacion_2=$2, evaluacion_3=$3, evaluacion_4=$4, nota=$5 
       WHERE evaluaciones.id_alumno=$6 
       AND evaluaciones.id_maestro=$7`;

    const values = [
      evaluation1,
      evaluation2,
      evaluation3,
      evaluation4,
      average,
      this.idStudent,
      this.idTeacher,
    ];
    try {
      const result = await Evaluations.findByIdStudent({ id: this.idStudent });
      if (result.length === 0) {
        return { error: 'el estudiante no existe' };
      }
      await queryHandler.exect(query, values);
      return {};
    } catch (error) {
      throw new Error('No  se pudo actualizar la evaluacion');
    }
  };

  private averageEvaluations = ({
    evaluation1,
    evaluation2,
    evaluation3,
    evaluation4,
  }: IEvaluation) => {
    const newEvaluation2 = evaluation2 ?? 0;
    const newEvaluation3 = evaluation3 ?? 0;
    const newEvaluation4 = evaluation4 ?? 0;
    const sumEvaluations =
      parseFloat(evaluation1.toString()) +
      parseFloat(newEvaluation2.toString()) +
      parseFloat(newEvaluation3.toString()) +
      parseFloat(newEvaluation4.toString());

    const average = sumEvaluations / 4;

    return average;
  };

  private static findByIdStudent = async ({ id }: { id: string }) => {
    const query = 'SELECT * FROM evaluaciones WHERE evaluaciones.id_alumno=$1';
    const values = [id];
    try {
      const queryResult = await queryHandler.exect(query, values);
      return queryResult.rows;
    } catch (error) {
      throw new Error(String(error));
    }
  };
}
