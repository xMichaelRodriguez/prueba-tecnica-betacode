import app from '.';

// eslint-disable-next-line no-console
const server = app.listen(app.get('port'), () =>
  console.log(`http://localhost:${app.get('port')}`)
);

export default server;
