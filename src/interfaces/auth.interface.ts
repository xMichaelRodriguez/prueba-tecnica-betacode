export interface IRegister {
  classRoom: number;
  course: string;
  email: string;
  name: string;
  password: string;
}

export interface IUser extends IRegister {
  id: string;
}
export interface IErrorOrSuccessAuth {
  msg?: string;
  ok: boolean;
  user?: IUser;
}

export interface ILogin {
  email: string;
  password: string;
}
export interface IResponse {
  found: boolean;
  valid: boolean;
  user: IUser | null;
}

export interface IAuth {
  uid: string;
  iat: number;
  exp: number;
}
