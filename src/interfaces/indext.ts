export * from './auth.interface';
export * from './student.interface';
export * from './evaluations.interface';
export interface IPayload {
  uid: string;
  name: string;
}
