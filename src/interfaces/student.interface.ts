export interface IStudent {
  id: string;
  name: string;
  email: string;
  phone: string;
  dutyManager: string;
}
export interface IPublicStudent {
  name: string;
  email: string;
  phone: string;
  dutyManager: string;
}
export interface IPublicStudentSpanish {
  id: string;
  nombre: string;
  email: string;
  tel: string;
  encargado: string;
}
