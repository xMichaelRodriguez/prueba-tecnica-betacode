import express, { Request, Response } from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import path from 'path';

// import routes
import authRoute from './routes/auth.routes';
import studentsRoutes from './routes/students.routes';
import evaluationRoutes from './routes/evaluation.routes';

// config
dotenv.config({ path: path.resolve(process.cwd(), './.env') });
const app = express();

const PORT = process.env.PORT || 3000;
app.set('port', PORT);
// middleware
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// routes
app.use('/api/v1/auth', authRoute);
app.use('/api/v1/students', studentsRoutes);
app.use('/api/v1/evaluations', evaluationRoutes);

app.use('*', (_: Request, res: Response) => res.status(404).jsonp(['endpoint no valido']));

// listen
export default app;
