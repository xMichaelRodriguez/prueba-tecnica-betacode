import { Pool } from 'pg';
import dotenv from 'dotenv';

dotenv.config();
const {
  HOST_DB,
  USER_DB,
  PASSWORD_DB,
  DATABASE_DB,
  PORT_DB,
  HOST_DB_TEST,
  USER_DB_TEST,
  PASSWORD_DB_TEST,
  DATABASE_DB_TEST,
  PORT_DB_TEST,
  NODE_ENV,
} = process.env;

const newPort = NODE_ENV === 'test' ? Number(PORT_DB_TEST) : Number(PORT_DB);
const newUser = NODE_ENV === 'test' ? String(USER_DB_TEST) : String(USER_DB);
const newHost = NODE_ENV === 'test' ? String(HOST_DB_TEST) : String(HOST_DB);
const newDb = NODE_ENV === 'test' ? String(DATABASE_DB_TEST) : String(DATABASE_DB);
const newPass = NODE_ENV === 'test' ? String(PASSWORD_DB_TEST) : String(PASSWORD_DB);

const pool = new Pool({
  user: newUser,
  host: newHost,
  database: newDb,
  password: newPass,
  port: newPort,
});
export default pool;
