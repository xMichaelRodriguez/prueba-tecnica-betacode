import pool from './pool';

pool.on('connect', () => {
  // eslint-disable-next-line no-console
  console.log('DB CONNECTED');
});

export default {
  exect: (query: string, values: (string | number)[]) => pool.query(query, [...values]),
};
