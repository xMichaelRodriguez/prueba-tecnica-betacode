/* eslint-disable consistent-return */
import supertest from 'supertest';

import app from '..';
import server from '../app';
// import pool from '../storage/pool';

const api = supertest(app);

describe('test in auth', () => {
  afterAll(() => server.close());

  it('should return status code 401 if there is a user with the provided email', (done) => {
    api
      .post('/api/v1/auth/register')
      .send({
        name: 'Michael',
        course: 'Rodriguez',
        classRoom: 1,
        email: 'scottlovos503@gmail.com',
        password: 'ThePassword123',
      })
      .expect(400)
      .end((err) => (err ? done(err) : done()));
  });

  // it('should return status code 201 if the user was registered', (done) => {
  //   api
  //     .post('/api/v1/auth/register')
  //     .send({
  //       name: 'PATRICO',
  //       course: 'Rodriguez',
  //       classRoom: 1,
  //       email: 'aliquam@google.net',
  //       password: 'ThePassword123',
  //     })
  //     .expect(201)
  //     .end((err) => (err ? done(err) : done()));
  // });

  it('return status code 200 if email and password is passed', (done) => {
    api
      .post('/api/v1/auth/login')
      .send({ email: 'scottlovos503@gmail.com', password: 'ThePassword123' })
      .expect(200)
      .end((err) => (err ? done(err) : done()));
  });
  it('successful login and return user data', async () => {
    const response = await api
      .post('/api/v1/auth/login')
      .send({ email: 'scottlovos503@gmail.com', password: 'ThePassword123' });

    expect(response.body).toReturnWith({
      user: {
        aula: 1,
        email: 'scottlovos503@gmail.com',
        id: '80b3d130-a9df-4c11-a9e1-e6da201aa66b',
        materia: 'backend',
        nombre: 'michael rodriguez',
      },
    });
  });
});
