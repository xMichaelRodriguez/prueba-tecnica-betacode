import { Router } from 'express';
import { check, param } from 'express-validator';
import jwtValidator from '../middleware/jwtValidator';

import validateInputs from '../middleware/validateInputs';
import validateNumberPhone from '../middleware/ValidateNumberPhone';

import studentsController from '../controllers/students/students.controller';

const router = Router();
const { deleteStudent, findById, getStudents, register, updateStudents } = studentsController;

router.get('/:student', jwtValidator, findById);
router.get('/', jwtValidator, getStudents);

router.patch(
  '/:student',
  [
    jwtValidator,
    param('student').exists().withMessage("parametro 'student' obligatorio"),
    check('name').not().isEmpty().trim().withMessage('Nombre de docente obligatorio'),
    check('email').isEmail().withMessage('Ingrese un correo valido'),
    check('phone')
      .not()
      .isEmpty()
      .withMessage('Telefono requerido')
      .custom(validateNumberPhone)
      .withMessage(
        "Por favor ingrese un numero de telefono valido, sugerencia:'6060-6060', '6060 6060'"
      ),
    check('dutyManager').not().isEmpty().trim().withMessage('Encargado de estudiante obligatorio'),
    validateInputs,
  ],
  updateStudents
);
router.post(
  '/register',
  [
    check('name').not().isEmpty().trim().withMessage('Nombre de docente obligatorio'),
    check('email').isEmail().withMessage('Ingrese un correo valido'),
    check('phone')
      .not()
      .isEmpty()
      .withMessage('Telefono requerido')
      .custom(validateNumberPhone)
      .withMessage(
        "Por favor ingrese un numero de telefono valido, sugerencia:'6060-6060', '6060 6060'"
      ),
    check('dutyManager').not().isEmpty().trim().withMessage('Encargado de estudiante obligatorio'),
    jwtValidator,
    validateInputs,
  ],
  register
);

router.delete('/:student', jwtValidator, deleteStudent);

export default router;
