import { Router } from 'express';
import { check } from 'express-validator';
import evaluationController from '../controllers/evaluations/evaluation.controller';
import upload from '../middleware/isUploadFile';
import jwtValidator from '../middleware/jwtValidator';
import validateInputs from '../middleware/validateInputs';

const router = Router();

const { uploadEvaluations, registerEvalation, updateEvaluation, getEvaluationsForTeacher } =
  evaluationController;
router.get('/', jwtValidator, getEvaluationsForTeacher);

router.post('/upload-evaluations', [jwtValidator, upload.single('evaluations')], uploadEvaluations);

router.post(
  '/:student',
  [
    jwtValidator,
    check('student').not().isEmpty().withMessage('student parametro obligatorio'),
    check('evaluation1').isNumeric().withMessage('Evaluacion 1 obligatoria'),
    check('evaluation2').optional({ checkFalsy: true }).escape().isNumeric(),
    check('evaluation3').optional({ checkFalsy: true }).escape().isNumeric(),
    check('evaluation4').optional({ checkFalsy: true }).escape().isNumeric(),
    validateInputs,
  ],
  registerEvalation
);

router.patch(
  '/:student',
  [
    jwtValidator,
    check('student').exists().withMessage('student parametro obligatorio'),
    check('evaluation1').isNumeric().withMessage('Evaluacion 1 obligatoria'),
    check('evaluation2').optional({ checkFalsy: true }).escape().isNumeric(),
    check('evaluation3').optional({ checkFalsy: true }).escape().isNumeric(),
    check('evaluation4').optional({ checkFalsy: true }).escape().isNumeric(),
    validateInputs,
  ],
  updateEvaluation
);

export default router;
