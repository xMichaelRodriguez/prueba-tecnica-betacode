import { Router } from 'express';
import { check } from 'express-validator';

import authController from '../controllers/auth/auth.controller';
import jwtValidator from '../middleware/jwtValidator';
import validateInputs from '../middleware/validateInputs';

const router = Router();
const { login, register, getProfile } = authController;

router.get('/', jwtValidator, getProfile);

router.post(
  '/register',
  [
    check('name').not().isEmpty().trim().withMessage('Nombre de docente obligatorio'),
    check('course').not().isEmpty().trim().withMessage('curso de docente obligatorio'),
    check('email').isEmail().withMessage('Ingrese un correo valido').trim(),
    check('classRoom')
      .not()
      .isEmpty()
      .withMessage('Aula de docente obligatoria')
      .trim()
      .isNumeric()
      .withMessage('debe ser un numero de 1 a 4'),
    check('password')
      .isLength({ min: 8 })
      .withMessage('contraseña debe contener minimo 8 caracteres ')
      .matches(/\d/)
      .withMessage('debe contener un numero'),
    validateInputs,
  ],
  register
);

router.post(
  '/login',
  [
    check('email').isEmail().withMessage('Ingrese un correo valido').trim(),
    check('password')
      .isLength({ min: 8 })
      .withMessage('contraseña debe contener minimo 8 caracteres ')
      .matches(/\d/)
      .withMessage('debe contener un numero'),
    validateInputs,
  ],
  login
);

export default router;
